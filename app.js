Ext.Loader.setConfig({
    enabled: true,
    disableCaching: true,
    paths: {
    	MyApp: 'app',
    }
});

Ext.require([
	"MyApp.view.ViewportController",
	"MyApp.model.Category",
	"MyApp.store.Category",
	"MyApp.model.Tasks",
	"MyApp.store.Tasks",
	"MyApp.view.Tasks",
	"MyApp.view.Charts",
	"MyApp.view.ChartsController",
	"MyApp.model.Log",
	"MyApp.store.Log",
	"MyApp.view.Log"]);

Ext.application({
	name: 'MyApp',
	autoCreateViewport: true
});
