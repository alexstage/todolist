<?php

	function error ($msg) {
		echo json_encode(Array('error' => $msg));
		exit;
	}

	$requestUri = explode('/', trim($_SERVER['REQUEST_URI'],'/'));
	if (count($requestUri) == 0) error("API не найден");
    $requestParams = $_REQUEST;

	$method = $_SERVER['REQUEST_METHOD'];
	
	if ($method == 'POST' && array_key_exists('HTTP_X_HTTP_METHOD', $_SERVER)) {
		
		if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE') $method = 'DELETE';
		else if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT') $this->method = 'PUT';
		else error("METHOD неизвестен");

	}

	if ($method == 'GET') { }
	if ($method == 'POST') { }
	if ($method == 'PUT') { }
	if ($method == 'DELETE') { }

	error("METHOD не определен");

?>