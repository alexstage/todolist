<?php

	include("property.php");

	// -------------------------------------------------------------
	// Обработка api запроса 'create' и создание новой записи в базе
	// -------------------------------------------------------------

	$m_success = array("success"=>true);

	// Проверка на наличие данных

	if ($_SERVER['REQUEST_METHOD'] != 'POST') { echo "Неверный запрос"; exit; }

	$m_post = json_decode($_POST['tasks'], true);
	if (isset($m_post['id'])) unset($m_post['id']);

	// Выбор файл или БД

	if ($GLOBALS['db'] == 'postgres') {

		$m_post = insertDB($dbconnect, $m_post);
		$m_tasks = readDB($dbconnect);

	}
	else {

		// Чтение в массивы базы json и новой записи из запроса

		$m_tasks = json_decode(file_get_contents("../data/task.json"), true);
		if (!isset($m_tasks['tasks'])) $m_tasks['tasks'] = array();

		// Выборка максимального значения поля 'task_id' из базы

		$task_id = 0;
		foreach ($m_tasks['tasks'] as $rowk=>$rowv) {
			if ((int)$rowv['task_id'] > $task_id) $task_id = (int)$rowv['task_id'];
		}

		// Установка значений полей новой записи

		$m_post['task_id'] = (string)($task_id + 1);
		$m_post['status'] = 'active';
		if (!is_string($m_post['category'])) $m_post['category'] = '';

		// Добавление новой записи в массив

		$m_tasks['tasks'][] = $m_post;

		// Запись обновленного массива в файл

		$file = fopen("../data/task.json", 'w');
		fwrite($file, json_encode($m_tasks, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		fclose($file);

	}

	// Вывод в поток ответа json данных из массивов

	$m_category = getPropertyUniqueList($m_tasks, 'tasks', 'category');
	$m_charts_status = getChartsStatus($m_tasks, 'tasks', 'charts_status');
	$m_charts_category = getChartsCategory($m_tasks, 'tasks', 'charts_category');
	$m_charts_tags = getChartsTags($m_tasks, 'tasks', 'charts_tags');

	$m_out = array("tasks"=>$m_post);
	insertLog($dbconnect,"create",$m_post);
	
	echo json_encode($m_success + $m_out + $m_category + $m_charts_status + $m_charts_category + $m_charts_tags, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

?>
