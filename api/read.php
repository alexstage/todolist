<?php

	include("property.php");

	// ---------------------------------------------------
	// Обработка api запроса 'read' и вывод ответа в поток
	// ---------------------------------------------------

	$m_success = array("success"=>true);

	// Выбор файл или БД

	if ($GLOBALS['db'] == 'postgres') {

		$m_tasks = readDB($dbconnect);

	}
	else {

		$m_tasks = json_decode(file_get_contents("../data/task.json"), true);
		if (!isset($m_tasks['tasks'])) $m_tasks['tasks'] = array();

	}

	// Вывод в поток ответа json данных из массивов

	$m_category = getPropertyUniqueList($m_tasks, 'tasks', 'category');
	$m_charts_status = getChartsStatus($m_tasks, 'tasks', 'charts_status');
	$m_charts_category = getChartsCategory($m_tasks, 'tasks', 'charts_category');
	$m_charts_tags = getChartsTags($m_tasks, 'tasks', 'charts_tags');

	// Вывод в поток ответа json данных из массивов 'tasks' и 'category'

	echo json_encode($m_success + $m_tasks + $m_category + $m_charts_status + $m_charts_category + $m_charts_tags, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

?>
