<?php

	include("property.php");

	// --------------------------------------------------
	// Обработка api запроса 'log' и вывод ответа в поток
	// --------------------------------------------------

	$m_success = array("success"=>true);

	if ($GLOBALS['db'] == 'postgres') {

		$m_log = readLogDB($dbconnect);

	}
	else {

		// Чтение файла json в массив

		$str = ltrim(file_get_contents("../data/log.json"));
		if (strlen($str) == 0) $str = '{}'; else $str[0] = " ";
		$m_log = json_decode("{\"log\":[".$str."]}", true);

	}

	// Вывод в поток ответа json данных из массива 'log'

	echo json_encode($m_success + $m_log, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

?>
