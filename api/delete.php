<?php

	include("property.php");

	// --------------------------------------------------------
	// Обработка api запроса 'delete' и удаление  записи в базе
	// --------------------------------------------------------

	$m_success = array("success"=>true);

	// Проверка на наличие данных

	if ($_SERVER['REQUEST_METHOD'] != 'POST') { echo "Неверный запрос"; exit; }

	$m_post = json_decode($_POST['tasks'], true);
	if (isset($m_post['id'])) unset($m_post['id']);

	// Выбор файл или БД

	if ($GLOBALS['db'] == 'postgres') {

		$m_post = deleteDB($dbconnect, $m_post);
		$m_tasks = readDB($dbconnect);

	}
	else {

		// Чтение в массивы базы json и запроса на удаление записи

		$m_tasks = json_decode(file_get_contents("../data/task.json"), true);
		if (!isset($m_tasks['tasks'])) $m_tasks['tasks'] = array();

		// Поиск в массиве из файла совпадения по полю 'task_id' и удаления записи

		$row = -1;
		foreach ($m_tasks['tasks'] as $rowk=>$rowv) {
			if ($rowv['task_id'] == $m_post['task_id']) $row = $rowk;
		}

		if ($row != -1) unset($m_tasks['tasks'][$row]);
		$m_tasks['tasks'] = array_values($m_tasks['tasks']);

		// Запись обновленного массива в файл

		$file = fopen("../data/task.json", 'w');
		fwrite($file, json_encode($m_tasks, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES));
		fclose($file);

	}

	// Вывод в поток ответа json данных из массивов

	$m_category = getPropertyUniqueList($m_tasks, 'tasks', 'category');
	$m_charts_status = getChartsStatus($m_tasks, 'tasks', 'charts_status');
	$m_charts_category = getChartsCategory($m_tasks, 'tasks', 'charts_category');
	$m_charts_tags = getChartsTags($m_tasks, 'tasks', 'charts_tags');

	$m_out = array("tasks"=>$m_post);
	insertLog($dbconnect,"delete",$m_post);

	echo json_encode($m_success + $m_out + $m_category + $m_charts_status + $m_charts_category + $m_charts_tags, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES);

?>
