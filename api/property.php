<?php
 
 	date_default_timezone_set("Europe/Moscow");

 // -----------------------------------------------------

	if (isset($_GET['db'])) $GLOBALS['db'] = $_GET['db'];
	else if (isset($_POST['db'])) $GLOBALS['db'] = $_POST['db'];

	if ($GLOBALS['db'] == 'postgres') {
	 	include ("db.php");
		$dbconnect = pg_connect($connect_string);
		$query = "
			CREATE TABLE IF NOT EXISTS tasks (
				task_id serial PRIMARY KEY,
				done boolean NOT NULL DEFAULT false,
				notes text,
				category text,
				date timestamp NOT NULL DEFAULT date_trunc('day', current_timestamp)
			);
		";
		$result = pg_query($dbconnect, $query);
		$query = "
			CREATE TABLE IF NOT EXISTS log (
				api character varying(7),
				datelog timestamp NOT NULL DEFAULT current_timestamp,
				task_id integer,
				status character varying(7),	
				notes text,
				category text,
				date timestamp
			);
		";
		$result = pg_query($dbconnect, $query);
	}

// -----------------------------------------------------

	function getPropertyUniqueList ($m_in, $root, $property) {

		if (!isset($m_in[$root])) $m_in[$root] = [];

		// Выборка значений поля $property

		$m = [];
		foreach ($m_in[$root] as $rowk=>$rowv) {
			if (is_string($rowv[$property]) && $rowv[$property] != "") {
				array_push($m, $rowv[$property]);
			}
		}

		// Удаление дубликатов

		$m = array_unique($m);

		// Формирование массива $property для записи в поток

		$m_out[$property] = [];
		foreach ($m as $value) {
			$m_out[$property][] = array("name"=>$value);
		}

		return $m_out;
	}

// -----------------------------------------------------

	function getChartsStatus ($m_in, $root, $property) {

		if (!isset($m_in[$root])) $m_in[$root] = [];

		$now = mktime(0,0,0);
		$m = [];
		foreach ($m_in[$root] as $rowk=>$rowv) {
			$status = $rowv['status'];
			$date = strtotime($rowv['date']);
			if ($status == 'active' && $date < $now) $ret = '1-expired';
			else if ($status == 'active' && $date >= $now) $ret = '2-active';
			else if ($status == 'done' && $date >= $now) $ret = '3-done';
			else if ($status == 'done' && $date < $now) $ret = '4-archive';
			if (!isset($m[$ret])) $m[$ret] = 1; else $m[$ret]++;
		}

		$m_name = array(
			'1-expired'=>'Просроченные',
			'2-active'=>'Активные',
			'3-done'=>'Готовые',
			'4-archive'=>'В архиве'
		);

		$m_out[$property] = [];
		foreach ($m as $sort=>$value) {
			$m_out[$property][] = array("sort"=>$sort,"name"=>$m_name[$sort],"value"=>$value);
		}

		return $m_out;

	}

// -----------------------------------------------------

	function getChartsCategory ($m_in, $root, $property) {

		if (!isset($m_in[$root])) $m_in[$root] = [];

		$m = [];
		foreach ($m_in[$root] as $rowk=>$rowv) {
			if (!isset($m[$rowv['category']])) $m[$rowv['category']] = 1; else $m[$rowv['category']]++;
		}


		$m_out[$property] = [];
		foreach ($m as $name=>$value) {
			if ($name != '') {
				$m_out[$property][] = array("name"=>$name,"value"=>$value);
			}
		}

		return $m_out;

	}

// -----------------------------------------------------

	function getChartsTags ($m_in, $root, $property) {

		if (!isset($m_in[$root])) $m_in[$root] = [];

		$m = [];
		foreach ($m_in[$root] as $rowk=>$rowv) {
			$tags = explode(" ", str_replace("#", "@", $rowv['notes']));
			foreach ($tags as $value) {
				if ($value[0] == '@') {
					$value = mb_strtoupper(str_replace("@", "", $value));
					if (!isset($m[$value])) $m[$value] = 1; else $m[$value]++;
				}
			}
		}

		$m_out[$property] = [];
		foreach ($m as $name=>$value) {
			if ($name != '') {
				$m_out[$property][] = array("name"=>$name,"value"=>$value);
			}
		}

		return $m_out;

	}

// -----------------------------------------------------

	function readLogDB ($connection) {

		$ret['log'] = array();
		$query = "SELECT * FROM log;";
		$result = pg_query($connection, $query);
		while ($row = pg_fetch_assoc($result)) {
			$ret['log'][] = $row;
		}
		return $ret;
	}

	function insertLog ($connection, $api, $record) {

		if ($GLOBALS['db'] == 'postgres') {

			$m_values = array('api'=>quote_smart(my_strip($api)));
			foreach ($record as $name=>$value) {
				$s = my_strip($value);
				if ($s == '') continue;
				$m_values[$name] = quote_smart($s);
			}
			$str_fields = implode(",", array_keys($m_values));
			$str_values = implode(",", $m_values);
			$query = "
				INSERT INTO log ({$str_fields}) VALUES ({$str_values});
			";
			$result = pg_query($connection, $query);

		}
		else {

			$m_log = array("api"=>$api, "datelog"=>date("c"));

			$file = fopen("../data/log.json", 'a+');
			fwrite($file, ",".json_encode($m_log + $record, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES)."\n");
			fclose($file);

		}

	}

// -----------------------------------------------------

	function readDB ($connection) {

		$query = "
			SELECT
				task_id,
				CASE WHEN done = true THEN 'done' ELSE 'active' END as status,
				COALESCE(notes, '') as notes,
				COALESCE(category, '') as category,
				date
			FROM tasks;
		";
		$ret['tasks'] = array();
		$result = pg_query($connection, $query);
		while ($row = pg_fetch_assoc($result)) {
			$ret['tasks'][] = $row;
		}
		return $ret;

	}

	function insertDB ($connection, $record) {

		$m_fields = array('notes','category','date');
		$m_values = array();
		foreach ($m_fields as $name) {
			$s = my_strip($record[$name]);
			if ($s == '') continue;
			$m_values[$name] = quote_smart($s);
		}
		$str_fields = implode(",", array_keys($m_values));
		$str_values = implode(",", $m_values);
		$query = "
			INSERT INTO tasks ({$str_fields}) VALUES ({$str_values})
			RETURNING
				task_id,
				CASE WHEN done = true THEN 'done' ELSE 'active' END as status,
				{$str_fields};
		";
		$result = pg_query($connection, $query);
		$row = pg_fetch_assoc($result);
		return $row;

	}

	function updateDB ($connection, $record) {

		$task_id = 0;
		$m_values = array();
		foreach ($record as $name=>$value) {
			$s = my_strip($value);
			if ($name == 'task_id') { $task_id = (int)$s; continue; }
			if ($name == 'status') {
				$name = 'done';
				if ($s == 'done') $s = 'true'; else $s = 'false';
			}
			$m_values[$name] = quote_smart($s);
		}
		$m_set = array();
		foreach ($m_values as $name=>$value) $m_set[] = $name." = ".$value;
		$str_set = implode(",", $m_set);
		$query = "
			UPDATE tasks SET {$str_set} WHERE task_id = {$task_id};
		";
		$result = pg_query($connection, $query);
		$query = "
			SELECT
				task_id,
				CASE WHEN done = true THEN 'done' ELSE 'active' END as status,
				COALESCE(notes, '') as notes,
				COALESCE(category, '') as category,
				date
			FROM tasks
			WHERE task_id = {$task_id};
		";
		$result = pg_query($connection, $query);
		$row = pg_fetch_assoc($result);
		return $row;

	}

	function deleteDB ($connection, $record) {

		$task_id = 0;
		if (isset($record['task_id'])) $task_id = (int)my_strip($record['task_id']);
		$query = "
			DELETE FROM tasks WHERE task_id = {$task_id}
			RETURNING
				task_id,
				CASE WHEN done = true THEN 'done' ELSE 'active' END as status,
				COALESCE(notes, '') as notes,
				COALESCE(category, '') as category,
				date;
		";
		$result = pg_query($connection, $query);
		$row = pg_fetch_assoc($result);
		return $row;

	}

// -----------------------------------------------------

	function my_strip($str) {

		$s = strip_tags($str);
		$s = trim($str);
		if (get_magic_quotes_gpc()) {
			$s = stripslashes($s);
		}
		return $s;

	}

// -----------------------------------------------------

	function quote_smart($value) {

		$value = "'" . pg_escape_string($value) . "'";
		return $value;

	}

// -----------------------------------------------------

?>
