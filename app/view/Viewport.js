// MyApp Viewport

Ext.define('MyApp.view.Viewport', {
	
	extend: 'Ext.container.Viewport',
	controller: 'viewport',
	//autoScroll: true,
	//autoHeight: true,
	padding: 50,
	items: [{
		xtype: 'container',
		layout: 'hbox',
		defaultType: 'button',
		margin: "0 0 20 0",
		items: [{
			text: 'Список задач',
			handler: 'onShowClick',
			reference: 'showTasks',
			padding: 10,
			disabled: true,
		}, {
			text: 'Состояние задач',
			margin: '0 0 0 10',
			handler: 'onShowClick',
			reference: 'showCharts',
			padding: 10
		}, {
			text: 'Журнал изменений',
			margin: '0 0 0 10',
			handler: 'onShowClick',
			reference: 'showLog',
			padding: 10
		}, {
			xtype: 'container',
			flex: 1
		}, {
			text: 'Файл JSON',
			margin: '0 0 0 10',
			handler: 'onSelectDB',
			reference: 'selectJSON',
			padding: 10,
			disabled: true
		}, {
			text: 'PostgreSQL',
			margin: '0 0 0 10',
			handler: 'onSelectDB',
			reference: 'selectPostgreSQL',
			padding: 10
		}]
	}, {
		xtype: 'container',
		margin: "0 0 20 0",
		items: [{
			xtype: 'Tasks',
			reference: 'widgetTasks'
		}, {
			xtype: 'Charts',
			reference: 'widgetCharts',
			hidden: true
		}, {
			xtype: 'Log',
			reference: 'widgetLog',
			hidden: true
		}]
 	}]

});
