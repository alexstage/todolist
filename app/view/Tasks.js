// Create components

var MyStoreTasks = Ext.create('MyApp.store.Tasks');
var MyStoreCategory = Ext.create('MyApp.store.Category');

var MyEditor = Ext.create('Ext.grid.plugin.RowEditing', {
	clicksToEdit: 2,
	listeners: {
		validateedit: function(editor, context, eOpts) {
			var date = context.newValues.date;
			var now = Ext.Date.clearTime(new Date());
			if (date < now) {
				Ext.Msg.show({
					title:'Создание задачи',
					msg: 'Дата выполнения задачи не может быть прошедшей',
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING
				});
				return false;
			}
		},
		edit: function (editor, context, eOpts) {
			var date = context.newValues.date;
			var now = Ext.Date.clearTime(new Date());
			if (context.newValues.category != 'Показать все') {
				MyStoreTasks.sync();
				if (MyCategory.getValue() == context.newValues.category) {
					MyCategory.select(context.newValues.category);
				}
			}
			else {
				MyStoreTasks.rejectChanges();
				if (context.record.phantom) {
					context.store.remove(context.record);
				}
			}
		},
		canceledit: function(editor, context, eOpts) {
			if (context.record.phantom) {
				context.store.remove(context.record);
			}
		}
	}
});

var MyCreate = { text: 'Создать', width: 100, iconCls: 'x-fa fa-plus-circle blue',
	handler: function () {
		MyEditor.cancelEdit();
		var rec = Ext.create('MyApp.model.Tasks', {
			'status': 'new',
			'notes': '',
			'category': '',
			'date': Ext.Date.clearTime(new Date())
		});
		MyStoreTasks.removeFilter('searchFilter');
		this.up().getComponent('search').setValue('');
		this.up('Tasks').getSelectionModel().select(0);
		this.up('Tasks').getView().refresh();
		MyStoreTasks.insert(0, rec);
		MyEditor.startEdit(rec, 1);
	}
};

var MyDelete = { text: 'Удалить', width: 100, iconCls: 'x-fa fa-trash red',
	handler: function () {
		MyEditor.cancelEdit();
		var selection = this.up('Tasks').getView().getSelectionModel().getSelection()[0];
		if (selection) {
			var value = selection.get('statusview');
			var notes = selection.get('notes');
			if (value == '2-active' || value == '3-done') {
				Ext.Msg.confirm({
					title:'Удаление задачи',
					msg: notes,
					buttons: Ext.Msg.OKCANCEL,
					icon: Ext.MessageBox.QUESTION,
					fn: function(buttonId, value, opt) {
						if (buttonId == 'ok') {
							MyStoreTasks.remove(selection);
							MyStoreTasks.sync();
							MyCategory.select('Показать все');
						}
				   }
				});
			}
			else {
				Ext.Msg.show({
					title:'Удаление задачи',
					msg: 'Возможно только для задач с непрошедшей датой',
					buttons: Ext.Msg.OK,
					icon: Ext.MessageBox.WARNING
				});
			}
		}
	}
};

var MyDone = { text: 'Готово', width: 100, iconCls: 'x-fa fa-check green',
	handler: function () {
		MyEditor.cancelEdit();
		var selection = this.up('Tasks').getView().getSelectionModel().getSelection()[0];
		if (selection) {
			var value = selection.get('statusview');
			if (value == '2-active') {
				selection.beginEdit();
				selection.set('status', 'done');
				selection.endEdit();
				MyStoreTasks.save();
			}
			else if (value == '3-done') {
				selection.beginEdit();
				selection.set('status', 'active');
				selection.endEdit();
				MyStoreTasks.save();
			}
			else {
				Ext.Msg.show({
				   title:'Статус выполнения',
				   msg: 'Изменить можно только для задач с непрошедшей датой',
				   buttons: Ext.Msg.OK,
				   icon: Ext.MessageBox.WARNING
				});
			}
		}
	}
};

var MyStatus = {
	xtype: 'combo',
	fieldLabel: 'Статус',
	labelAlign: 'right',
	width: 200,
	labelWidth: 50,
	name: 'status',
	displayField: 'name',
	fields: ['status','name','icon'],
	valueField: 'name',
	queryMode:'local',
	//matchFieldWidth: false,
	store: [
		[ 'all', 'Показать все', 'x-fa fa-align-justify blue' ],
		[ '1-expired', 'Просроченные', 'x-fa fa-clock-o red' ],
		[ '2-active','Активные','x-fa fa-clock-o blue' ],
		[ '3-done', 'Готовые', 'x-fa fa-check green' ],
		[ '4-archive', 'В архиве', 'x-fa fa-archive archive' ]
	],	
	editable: false,
	listeners: {
		afterrender: function() {
			this.setValue('all');
		},
		change: function(field, newValue) {
			MyEditor.cancelEdit();
			MyStoreTasks.removeFilter('statusFilter');
			var statusFilter = Ext.create('Ext.util.Filter',{
				id: 'statusFilter', property: 'statusview', caseSensitive: false, value: null
			});				
			if (newValue != '' && newValue != 'all') {
				statusFilter.setValue(newValue);
				MyStoreTasks.addFilter(statusFilter);
			}
		}
	}
};

var MyCategory = Ext.create('Ext.form.field.ComboBox', {
	fieldLabel: 'Категория',
	labelAlign: 'right',
	width: 300,
	labelWidth: 80,
	name: 'category',
	fields: ['name'],
	displayField: 'name',
	valueField: 'name',
	value: 'Показать все',
	queryMode:'local',
	store: MyStoreCategoryFilter,
	editable: false,
	listeners:{
		change: function(field, newValue) {
			MyEditor.cancelEdit();
			MyStoreTasks.removeFilter('categoryFilter');
			var categoryFilter = Ext.create('Ext.util.Filter',{
				id: 'categoryFilter', property: 'category', caseSensitive: false, value: null
			});				
			if (newValue != '' && newValue != 'Показать все') {
				categoryFilter.setValue(newValue);
				MyStoreTasks.addFilter(categoryFilter);
			}
		}
	}
});

var MySearch = {
	xtype: 'textfield',
	fieldLabel: 'Поиск',
	labelAlign: 'right',
	flex: 1,
	labelWidth: 50,
	itemId: 'search',
	listeners: {
		change: function(field, newValue) {
			MyEditor.cancelEdit();
			MyStoreTasks.removeFilter('searchFilter');
			if (newValue && newValue.length > 0) {
				var tags = newValue.toLowerCase().split(/[,\s]+/);
				var searchFilter = Ext.create('Ext.util.Filter', {
					id: 'searchFilter',
					filterFn: function(item) {
						var ret = false;
						Ext.Array.each(tags, function(name) {
							if (name.length > 0 && item.data.notes.toLowerCase().indexOf(name) != -1) {
								ret = true;
							}
						});
						return ret;
					}
				});
				MyStoreTasks.addFilter(searchFilter);
			}
		}
	}
}

// Tasks Grid Panel

Ext.define('MyApp.view.Tasks', {

	extend: 'Ext.grid.Panel',
	alias: 'widget.Tasks',
	store: MyStoreTasks,
	title: 'Список задач',
	border: true,
	forceFit: true,
	selType: 'rowmodel',
	plugins: [ MyEditor , 'gridfilters' ],
	height: 500,
	dockedItems: [{
		xtype: 'toolbar',
		layout: { padding: '5' },
		items: [
			MyCreate, '-',
			MyDelete, '-',
			MyDone, '-',
			MyStatus, '-',
			MyCategory, '-',
			MySearch
		],
	}],
	columns: [{
		xtype: 'actioncolumn',
		dataIndex: 'statusview',
		text: 'Статус',
		width: 100,
		align: 'center',
		sortable: true,
		hideable: false,
		renderer: function(value,meta,record) {
			if (value == '1-expired') this.iconCls = 'x-fa fa-clock-o red';
			else if (value == '2-active') this.iconCls = 'x-fa fa-clock-o blue';
			else if (value == '3-done') this.iconCls = 'x-fa fa-check green';
			else if (value == '4-archive') this.iconCls = 'x-fa fa-archive archive';
			else this.iconCls = '';
		}
	},
	{
		dataIndex: 'task_id',
		text: '№',
		width: 50,
		align: 'center',
		sortable: true,
		hideable: false,
		renderer: function(value,meta,record) {
			if (!isNaN(value)) return value;
		}
	},
	{
		dataIndex: 'notes',
		text: 'Задача',
		sortable: true,
		hideable: false,
		flex: 1,
		filter: {
			type: 'string'
		},
		editor: {
			xtype: 'textfield',
			allowBlank: true
		}
	},
	{
		dataIndex: 'category',
		text: 'Категория',
		width: 250,
		align: 'center',
		sortable: true,
		hideable: false,
		editor: {
			xtype: 'combo',
			name: 'category',
			displayField: 'name',
			fields: ['name'],
			valueField: 'name',
			queryMode:'local',
			store: MyStoreCategory,
			editable :true,
			forceSelection: false,
 		}
	},
	{
		dataIndex: 'date',
		text: 'Дата',
		width: 120,
		align: 'center',
		sortable: true,
		hideable: false,
		xtype: 'datecolumn',
		format: 'd/m/Y',
		editor: {
			xtype: 'datefield',
			allowBlank: false,
			displayField: 'date',
			fields: ['date'],
			valueField: 'date',
			queryMode:'local',
			format: 'd/m/Y',
		}
	}]
});
