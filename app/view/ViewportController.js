Ext.define('MyApp.view.ViewportController', {

	extend: 'Ext.app.ViewController',
	alias: 'controller.viewport',

	onShowClick: function (button) {

		var me = this,
			name = button.itemId,
			view = me.getView(),
			showTasks = me.lookup('showTasks'),
			showCharts = me.lookup('showCharts'),
			showLog = me.lookup('showLog'),
			widgetTasks = me.lookup('widgetTasks'),
			widgetCharts = me.lookup('widgetCharts'),
			widgetLog = me.lookup('widgetLog');

		if (button === showTasks) {
			showCharts.enable();
			showLog.enable();
			widgetTasks.show();
			widgetCharts.hide();
			widgetLog.hide();
		}
		else if (button === showCharts) {
			showTasks.enable();
			showLog.enable();
			widgetTasks.hide();
			widgetCharts.show();
			widgetLog.hide();
		} else {
			showTasks.enable();
			showCharts.enable();
			widgetTasks.hide();
			widgetCharts.hide();
			MyStoreLog.load();
			widgetLog.show();
		}

        button.disable();

	},

	onSelectDB: function (button) {

		var me = this,
			name = button.itemId,
			view = me.getView(),
			selectJSON = me.lookup('selectJSON'),
			selectPostgreSQL = me.lookup('selectPostgreSQL');

		if (button === selectJSON) {
			selectPostgreSQL.enable();
			MyStoreTasks.proxy.setExtraParams({ db: 'json' });
			MyStoreLog.proxy.setExtraParams({ db: 'json' });
			MyStoreTasks.reload();
			MyStoreLog.reload();
		} else {
			selectJSON.enable();
			MyStoreTasks.proxy.setExtraParams({ db: 'postgres' });
			MyStoreLog.proxy.setExtraParams({ db: 'postgres' });
			MyStoreTasks.reload();
			MyStoreLog.reload();
		}

        button.disable();

	},


});