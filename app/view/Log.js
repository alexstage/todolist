var MyStoreLog = Ext.create('MyApp.store.Log');

var MyLogColumn1 = [{

	dataIndex: 'datelog',
	text: 'Дата действия',
	width: 150,
	align: 'center',
	sortable: true,
	hideable: false,
	xtype: 'datecolumn',
	format: 'd/m/Y H:i:s'
},
{
	dataIndex: 'api',
	text: 'Действие',
	flex: 1,
	sortable: false,
	hideable: false,
	renderer: function(value,config,renderData) {
		var str;
		if (value == 'create') str = '<i>Создание:</i>';
		else if (value == 'update') str = '<i>Изменение:</i>';
		else if (value == 'delete') str = '<i>Удаление:</i>';
		Ext.each(renderData.data, function (field) {
			if (field.task_id) str += ' <b>task_id</b> - (' + field.task_id + ');';
			if (field.status) str += ' <b>status</b> - (' + field.status + ');';
			if (field.date) str += ' <b>date</b> - (' + Ext.Date.format(field.date, 'd/m/Y') + ');';
			if (field.category) str += ' <b>category</b> - (' + field.category + ');';
			if (field.notes) str += ' <b>notes</b> - (' + field.notes + ');';
		});
		return str;
	}
}];

var MyLogColumn2 = [{

	dataIndex: 'datelog',
	text: 'Дата действия',
	width: 150,
	align: 'center',
	sortable: true,
	hideable: false,
	xtype: 'datecolumn',
	format: 'd/m/Y H:i:s'
},
{
	dataIndex: 'api',
	text: 'Действие',
	width: 150,
	align: 'center',
	sortable: false,
	hideable: false,
	renderer: function(value,config,renderData) {
		var str;
		if (value == 'create') str = 'Создание';
		else if (value == 'update') str = 'Изменение';
		else if (value == 'delete') str = 'Удаление';
		return str;
	}
},
{
	text: 'Изменения',
	flex: 1,
	columns: [{
		dataIndex: 'task_id',
		text: '№ задачи',
		width: 100,
		align: 'center',
		sortable: true,
		hideable: true
	},
	{
		dataIndex: 'status',
		xtype: 'actioncolumn',
		text: 'Статус',
		width: 100,
		align: 'center',
		sortable: false,
		hideable: false,
		renderer: function(value,meta,record) {
			if (value == 'active') this.iconCls = 'x-fa fa-clock-o blue';
			else if (value == 'done') this.iconCls = 'x-fa fa-check green';
			else this.iconCls = '';
		}
	},
	{
		dataIndex: 'notes',
		text: 'Задача',
		flex: 0.8,
		sortable: false,
		hideable: false,
	},
	{
		dataIndex: 'category',
		text: 'Категория',
		flex: 0.2,
		align: 'center',
		sortable: false,
		hideable: true,
	},
	{
		dataIndex: 'date',
		text: 'Дата',
		width: 100,
		align: 'center',
		sortable: false,
		hideable: true,
		xtype: 'datecolumn',
		format: 'd/m/Y'
	}]
}];

Ext.define('MyApp.view.Log', {

	extend: 'Ext.grid.Panel',
	alias: 'widget.Log',
	store: MyStoreLog,
	title: 'Журнал изменений',
	autoScroll: true,
	height: 500,
	dockedItems: [{
		xtype: 'toolbar',
		layout: { padding: '5' },
		items: [{
			text: 'Вариант 1', width: 100,
			handler: function () {
				this.up('Log').getView().grid.reconfigure(MyStoreLog, MyLogColumn1);
			}
		},
		{
			text: 'Вариант 2', width: 100,
			handler: function () {
				this.up('Log').getView().grid.reconfigure(MyStoreLog, MyLogColumn2);
			}
		},
		{
			xtype: 'container',
			flex: 1
		},
		{
			text: 'Очистить журнал', width: 150,  iconCls: 'x-fa fa-trash red',
			handler: function () {
				var db = MyStoreLog.proxy.getExtraParams().db;
				var name = 'Файл JSON';
				if (db == 'postgres') name = 'PostgreSQL';
				Ext.Msg.confirm({
					title:'Очистка журнала',
					msg: 'Подтвердите очистку журнала &laquo' + name + '&raquo',
					buttons: Ext.Msg.OKCANCEL,
					icon: Ext.MessageBox.QUESTION,
					fn: function(buttonId, value, opt) {
						if (buttonId == 'ok') {
							Ext.Ajax.request({
								url: 'api/log_reset.php?db=' + db
							});
							MyStoreLog.reload();
						}
				   }
				});
			}
		}]
	}],
	columns: MyLogColumn1

});
