var MyStoreChartsStatus = Ext.create('Ext.data.Store', {
	fields: [ 'sort', 'name', 'value' ],
	sorters: [ { property: 'sort', direction: 'ASC' } ]
});

var MyStoreChartsCategory = Ext.create('Ext.data.Store', {
	fields: [ 'sort', 'name', 'value' ],
	sorters: [ { property: 'name', direction: 'ASC' } ]
});

var MyStoreChartsTags = Ext.create('Ext.data.Store', {
	fields: [ 'sort', 'name', 'value' ],
	sorters: [ { property: 'name', direction: 'ASC' } ]
});

Ext.define('MyApp.view.Charts', {

	extend: 'Ext.Panel',
	controller: 'charts',
	alias: 'widget.Charts',
	title: 'Состояние задач',
	autoHeight: true,
	autoScroll: true,
	border: true,
	layout: 'hbox',
	items: [{

		xtype: 'container',
 		width: '40%',
 		lyaout: 'vbox',
		items: [{
			xtype: 'container',
			layout: 'hbox',
			defaultType: 'button',
			padding: "20 20 0 20",
			items: [{
				text: 'Статус',
				width: '33%',
				handler: 'onShowClick',
				reference: 'showStatus',
				padding: 7,
				disabled: true,
			}, {
				text: 'Категории',
				width: '33%',
				margin: '0 0 0 10',
				handler: 'onShowClick',
				reference: 'showCategory',
				padding: 7
			}, {
				text: 'Метки',
				width: '33%',
				margin: '0 0 0 10',
				handler: 'onShowClick',
				reference: 'showTags',
				padding: 7
			}]
		}, {
			xtype: 'grid',
			reference: 'gridCharts',
			width: '100%',
			padding: 20,
			store: MyStoreChartsStatus,
			columns: [{
				dataIndex: 'name',
				text: 'Название',
				flex: 0.8,
				sortable: false,
				hideable: false,
			}, {
				dataIndex: 'value',
				text: 'Всего',
				align: 'center',
				flex: 0.2,
				sortable: false,
				hideable: false,
			}]
		}]
	}, {

		xtype: 'polar',
		reference: 'polarCharts',
		colors: ['red','blue','mediumseagreen','green'],
 		width: '60%',
		minHeight: 500,
        insetPadding: 40,
        innerPadding: 20,
		legend: {
			docked: 'right',
			margin: '20 40 20 0'
		},
		interactions: ['rotate', 'itemhighlight'],
		store: MyStoreChartsStatus,
		series: {
			type: 'pie',
			highlight: true,
			angleField: 'value',
			donut: 40,
			label: {
				field: 'name',
				display: 'rotate',
				// renderer: function(value,config,renderData) {
				// 	if (value == '1-expired') value = 'Просроченные';
				// 	else if (value == '2-active') value = 'Активные';
				// 	else if (value == '3-done') value = 'Готовые';
				// 	else if (value == '4-archive') value = 'В архиве';
				// 	return value;
				// }
			},
			// var total = 0;
			// MyStoreCharts.each(function(rec) {
			//     total += rec.get('data');
			// });
			// this.setTitle(store.get('name') + ': ' + Math.round(store.get('data') / total * 100) + '%');
		}

	}]

});
