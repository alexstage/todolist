Ext.define('MyApp.view.ChartsController', {

	extend: 'Ext.app.ViewController',
	alias: 'controller.charts',

	onShowClick: function (button) {

		var me = this,
			name = button.itemId,
			view = me.getView(),
			showStatus = me.lookup('showStatus'),
			showCategory = me.lookup('showCategory'),
			showTags = me.lookup('showTags'),
			gridCharts = me.lookup('gridCharts'),
			polarCharts = me.lookup('polarCharts'),
			store = gridCharts.getStore(),
			sorters = store.getSorters();

		if (button === showStatus) {

			gridCharts.setStore(MyStoreChartsStatus);
			polarCharts.setStore(MyStoreChartsStatus);
			showCategory.enable();
			showTags.enable();

		}
		else if (button === showCategory) {

			gridCharts.setStore(MyStoreChartsCategory);
			polarCharts.setStore(MyStoreChartsCategory);
			showStatus.enable();
			showTags.enable();

		} else {

			gridCharts.setStore(MyStoreChartsTags);
			polarCharts.setStore(MyStoreChartsTags);
			showStatus.enable();
			showCategory.enable();

		}

        button.disable();

	},

});
