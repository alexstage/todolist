MyStoreCategoryFilter = Ext.create('Ext.data.Store', {
	model: 'MyApp.model.Category',
});

Ext.define('MyApp.store.Category', {
	extend: 'Ext.data.Store',
	model: 'MyApp.model.Category',
	sorters: {
		property: 'name',
		direction: 'ASC'
	},
	listeners: {
		datachanged: function(store) {
			MyStoreCategoryFilter.removeAll();
			MyStoreCategoryFilter.add({ name: 'Показать все' });
			store.each(function(rec) {
				MyStoreCategoryFilter.add({ name: rec.data.name });
			});
		}
	}
});
