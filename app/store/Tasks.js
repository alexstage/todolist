Ext.define('MyApp.store.Tasks', {
	extend: 'Ext.data.Store',
	model: 'MyApp.model.Tasks',
	autoLoad: true,
	autoSync: false,
	sorters: [
		{ property: 'statusview', direction: 'ASC' },
	],
	proxy: {
		type: 'ajax',

		api: {
			create: 'api/create.php',
			read: 'api/read.php',
			update: 'api/update.php',
			destroy: 'api/delete.php'
		},
		extraParams: {
			db: 'json'
		},
		reader: {
			type: 'json',
			rootProperty: 'tasks',
		},
		writer: {
			type: 'json',
			rootProperty: 'tasks',
			encode: true,
			writeAllFields: true
		}
	},
	listeners: {
		load: function (store, records, successful, operation) {
			var response = Ext.JSON.decode(operation.getResponse().responseText);
			MyStoreCategory.loadData(response.category);
			MyStoreChartsStatus.loadData(response.charts_status);
			MyStoreChartsCategory.loadData(response.charts_category);
			MyStoreChartsTags.loadData(response.charts_tags);
		},
		write: function(store, operation) {
			var response = Ext.JSON.decode(operation.getResponse().responseText);
			MyStoreCategory.loadData(response.category);
			MyStoreChartsStatus.loadData(response.charts_status);
			MyStoreChartsCategory.loadData(response.charts_category);
			MyStoreChartsTags.loadData(response.charts_tags);
		}
	}
});
