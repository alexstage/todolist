Ext.define('MyApp.store.Log', {
	extend: 'Ext.data.Store',
	model: 'MyApp.model.Log',
	autoLoad: false,
	autoSync: false,
	sorters: [
		{ property: 'datelog', direction: 'DESC' },
	],
	proxy: {
		type: 'ajax',
		url: 'api/log.php',
		extraParams: {
			db: 'json'
		},
		reader: {
			type: 'json',
			rootProperty: 'log',
		}
	}
});
