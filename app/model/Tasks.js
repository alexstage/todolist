Ext.define('MyApp.model.Tasks', {
	extend: 'Ext.data.Model',
	idProperty: 'task_id',
	fields: [
		'task_id',
		'status',
		'notes',
		'category',
		{
			name: 'date',
			type: 'date',
			dateFormat: 'c'
		},
		{
			name: 'statusview',
			calculate: function (data) {
				var ret = '';
				var status = data.status;
				var date = data.date;
				var now = Ext.Date.clearTime(new Date());
				if (status == 'active' && date < now) ret = '1-expired';
				else if (status == 'active' && date >= now) ret = '2-active';
				else if (status == 'done' && date >= now) ret = '3-done';
				else if (status == 'done' && date < now) ret = '4-archive';
				return ret;
			}
		}
	]
});
