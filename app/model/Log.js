Ext.define('MyApp.model.Log', {
	extend: 'Ext.data.Model',
	//idProperty: 'pid',
	fields: [
		{ name: 'api' },
		{ name: 'datelog', type: 'date', dateFormat: 'c' },
		{ name: 'task_id' },
		{ name: 'status' },
		{ name: 'notes' },
		{ name: 'category' },
		{ name: 'date', type: 'date', dateFormat: 'c' }
	]
});
