Ext.define('MyApp.model.Category', {
	extend: 'Ext.data.Model',
	fields: [ 'name' ]
});
